import boto3


def main():
    for region in get_enabled_regions():
        print(f"Attempting to enable Access Analyzer in {region}.")
        access_analyzer = boto3.client(service_name="accessanalyzer", region_name=region)
        try:
            response = access_analyzer.create_analyzer(
                analyzerName=f"analyzer-{region}",
                type="ACCOUNT"
            )
        except access_analyzer.exceptions.ServiceQuotaExceededException as e:
            print(f"Access Analyzer already enabled in {region}.")
            continue
        status = get_status(response)
        print(status)


def get_enabled_regions():
    ec2 = boto3.client("ec2")
    response = ec2.describe_regions(
        Filters=[
            {
                "Name": "opt-in-status",
                "Values": [
                    "opt-in-not-required",
                    "opted-in"
                ]
            }
        ]
    )
    regions = [region["RegionName"] for region in response["Regions"]]
    return regions


def get_status(response):
    status = "SUCCESS!" if response["ResponseMetadata"]["HTTPStatusCode"] == 200 else "FAILED!"
    return status


if __name__ == "__main__":
    main()
