"""
© 2020 Amazon Web Services, Inc. or its affiliates. All Rights Reserved.
This AWS Content is provided subject to the terms of the AWS Customer
Agreement available at http://aws.amazon.com/agreement or other written
agreement between Customer and either Amazon Web Services, Inc.
or Amazon Web Services EMEA SARL or both.
"""
from json import dumps
from time import sleep
import logging
import math
import boto3

accessanalyzer = boto3.client("accessanalyzer")
logger = logging.getLogger()
logger.setLevel(logging.INFO)


def lambda_handler(event, context):
    """AWS Lambda handler for Python"""
    event_name = get_event_name(event)
    status = get_status(event_name)

    request_params = get_request_parameters(event)

    analyzer_name = get_analyzer_name(request_params)
    logger.info(f"Analyzer Name: {analyzer_name}")

    analyzer_arn = get_analyzer_arn(analyzer_name)
    logger.info(f"Analyzer ARN: {analyzer_arn}")

    analyzer_filter = get_filter(request_params)
    logger.info(f"Archive Rule:\n {dumps(analyzer_filter)}")

    findings = get_active_findings(analyzer_arn, analyzer_filter)
    logger.info(f"Number of finding(s): {len(findings)}")

    finding_ids = get_finding_ids(findings)
    logger.info(f"Finding ID(s): {dumps(format_finding_ids(finding_ids))}")

    archive_findings(analyzer_arn, finding_ids, status)


def get_event_name(event):
    """Returns eventName object from EventBridge event"""
    return event["detail"]["eventName"]


def get_status(event_name):
    """Returns ARCHIVED if EventBridge rule was valid"""
    if event_name in ["CreateArchiveRule", "UpdateArchiveRule"]:
        return "ARCHIVED"
    raise InvalidEventNameException(event_name)


def get_request_parameters(event):
    """Returns requestParameters object from EventBridge event"""
    return event["detail"]["requestParameters"]


def get_analyzer_name(request_params):
    """Returns analyzerName attribute from request_params"""
    return request_params["analyzerName"]


def get_analyzer_arn(analyzer_name):
    """
    Returns Access Analyzer arn from the analyzer name

        Parameters:
            analyzer_name (string): analyzer name string

        Returns:
            analyzerArn (string): analyzer arn
    """
    response = accessanalyzer.get_analyzer(
        analyzerName=analyzer_name
    )
    return response["analyzer"]["arn"]


def get_filter(request_params):
    """Returns Archive Rule filter from request_params"""
    return request_params["filter"]


def get_active_findings(analyzer_arn, analyzer_filter):
    """
    Returns list of active findings matching filter pattern

        Parameters:
            `analyzer_arn` (string): analyzer arn string
            `analyzer_filter` (dict): analyzer filter dict

        Returns:
            `active_findings` (list): active findings
    """
    findings = []
    paginator = accessanalyzer.get_paginator("list_findings")
    response_iterator = paginator.paginate(
        analyzerArn=analyzer_arn,
        filter=analyzer_filter
    )
    for page in response_iterator:
        findings += page["findings"]
    active_findings = [
        finding
        for finding in findings
        if finding["status"] == "ACTIVE"
    ]
    if len(active_findings) == 0:
        raise NoFindingsFoundException()
    return active_findings


def get_finding_ids(findings):
    """Returns a list with a single inner list of finding ids"""
    finding_ids = [finding["id"] for finding in findings]
    return [finding_ids]


def format_finding_ids(finding_ids):
    """Formats the finding ids for logging"""
    assembled_ids = []
    for ids in finding_ids:
        assembled_ids += ids
    id_format = {
        "ids": assembled_ids
    }
    return id_format


def partition_finding_ids(finding_ids):
    """Partition findings if there are a large number of findings"""
    while len(finding_ids[0]) > 50:
        partitioned_ids = []
        for sub_ids in finding_ids:
            middle = math.ceil(len(sub_ids) / 2)
            left, right = sub_ids[:middle], sub_ids[middle:]
            partitioned_ids.append(left)
            partitioned_ids.append(right)
        finding_ids = partitioned_ids
    return finding_ids


def archive_findings(analyzer_arn, finding_ids, status):
    """
    Archives findings and recursively retries if throttled

        Parameters:
            `analyzer_arn` (string): analyzer arn value
            `findings_ids` (list[list]): partitioned list of findings
            status (string): Archive Findings status
    """
    try:
        partitioned_ids = partition_finding_ids(finding_ids)
        for _ in range(len(partitioned_ids)):
            accessanalyzer.update_findings(
                analyzerArn=analyzer_arn,
                ids=partitioned_ids[0],
                status=status
            )
            partitioned_ids.pop(0)
    except accessanalyzer.exceptions.ThrottlingException:
        sleep(5)
        archive_findings(analyzer_arn, partitioned_ids, status)


class InvalidEventNameException(Exception):
    """Exception raised for invalid eventName from CloudTrail

    Attributes:
        event_name -- event name which caused the error
        message -- explanation of the error
    """

    def __init__(self, event_name,
        message="eventName can only be CreateArchiveRule or UpdateArchiveRule"):
        self.event_name = event_name
        self.message = message
        super().__init__(self.message)


class NoFindingsFoundException(Exception):
    """Exception raised for no matching findings

    Attributes:
        message -- explanation of the error
    """

    def __init__(self, message="Filter returned no matching findings"):
        self.message = message
        super().__init__(self.message)
