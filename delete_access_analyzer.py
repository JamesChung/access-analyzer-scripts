import boto3


def main():
    for region in get_enabled_regions():
        print(f"Attempting to delete Access Analyzer in {region}.")
        access_analyzer = boto3.client(service_name="accessanalyzer", region_name=region)    
        response = access_analyzer.list_analyzers(
            type="ACCOUNT"
        )
        try:
            analyzer_name = get_analyzer_name(response)
        except IndexError as e:
            continue
        response = access_analyzer.delete_analyzer(analyzerName=analyzer_name)
        status = get_status(response)
        print(status)


def get_enabled_regions():
    ec2 = boto3.client("ec2")
    response = ec2.describe_regions(
        Filters=[
            {
                "Name": "opt-in-status",
                "Values": [
                    "opt-in-not-required",
                    "opted-in"
                ]
            }
        ]
    )
    regions = [region["RegionName"] for region in response["Regions"]]
    return regions


def get_analyzer_name(response):
    analyzer_name = response["analyzers"][0]["name"]
    return analyzer_name


def get_status(response):
    status = "SUCCESS!" if response["ResponseMetadata"]["HTTPStatusCode"] == 200 else "FAILED!"
    return status


if __name__ == "__main__":
    main()
