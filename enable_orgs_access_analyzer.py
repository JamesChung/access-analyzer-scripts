import boto3


# Role with permissions needed to enable Access Analyzer.
# Constant: This role should exist in all accounts.
ROLE_NAME = "some/Role"

def main():
    sts = boto3.client("sts")
    current_account = sts.get_caller_identity()["Account"]
    accounts = get_active_accounts()
    roles = create_role_arns(accounts=accounts, role_name=ROLE_NAME)
    for role in roles:
        account = role["Account"]
        if account != current_account:
            assumed_role = sts.assume_role(
                RoleArn=role["arn"],
                RoleSessionName="AssumeRoleSession"
            )
            role_credentials = assumed_role["Credentials"]
        else:
            role_credentials = None
        for region in get_enabled_regions(assumed_role=role_credentials):
            print(f"Attempting to enable Access Analyzer for Account: {account} in {region}.")
            if account == current_account:
                access_analyzer = boto3.client(
                    service_name="accessanalyzer",
                    region_name=region
                )
            else:
                access_analyzer = boto3.client(
                    service_name="accessanalyzer",
                    region_name=region,
                    aws_access_key_id=role_credentials["AccessKeyId"],
                    aws_secret_access_key=role_credentials["SecretAccessKey"],
                    aws_session_token=role_credentials["SessionToken"]
                )
            try:
                response = access_analyzer.create_analyzer(
                    analyzerName=f"analyzer-{region}",
                    type="ACCOUNT"
                )
            except sts.exceptions.RegionDisabledException as sts_e:
                print(str(sts_e))
                continue
            except access_analyzer.exceptions.ServiceQuotaExceededException:
                print(f"Access Analyzer already enabled for Account: {account} in {region}.")
                continue
            except Exception as e:
                print(f"Unexpected error for Account: {account} in {region}!\n{e}")
                raise
            status = get_status(response)
            print(status)
        print(f"Enabled Access Analyzers in enabled regions for Account: {account}")
    print("Done!")


def get_all_accounts():
    organizations = boto3.client("organizations")
    response = organizations.list_accounts()
    accounts = [account for account in response["Accounts"]]
    return accounts


def get_active_accounts():
    accounts = get_all_accounts()
    active_accounts = []
    for account in accounts:
        if account["Status"] == "ACTIVE":
            active_accounts.append(account["Id"])
    return active_accounts


def create_role_arn(account_id, role_name):
    return f"arn:aws:iam::{account_id}:role/{role_name}"


# Assumes that the role name is the same across all accounts
def create_role_arns(accounts, role_name):
    roles = []
    for account in accounts:
        roles.append(
            {
                "Account": account,
                "arn": create_role_arn(account, role_name)
            }
        )
    return roles


def get_enabled_regions(assumed_role):
    if assumed_role is None:
        ec2 = boto3.client("ec2")
    else:
        ec2 = boto3.client(
            service_name="ec2",
            aws_access_key_id=assumed_role["AccessKeyId"],
            aws_secret_access_key=assumed_role["SecretAccessKey"],
            aws_session_token=assumed_role["SessionToken"]
        )
    response = ec2.describe_regions(
        Filters=[
            {
                "Name": "opt-in-status",
                "Values": [
                    "opt-in-not-required",
                    "opted-in"
                ]
            }
        ]
    )
    regions = [region["RegionName"] for region in response["Regions"]]
    return regions


def get_status(response):
    status = "SUCCESS!" if response["ResponseMetadata"]["HTTPStatusCode"] == 200 else "FAILED!"
    return status


if __name__ == "__main__":
    main()
